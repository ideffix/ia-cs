﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BooksClient.ServiceReference1;

namespace BooksClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            book[] books = useProperMethod((string) comboBox1.SelectedItem);
            dataGridView1.DataSource = books;
        }

        private book[] useProperMethod(string selectedValue)
        {
            string s = textBox1.Text;
            BookManagerClient bm = new BookManagerClient();
            switch (selectedValue)
            {
                case "Tytuł":
                    return bm.searchByTitle(s);
                case "Autor":
                    return bm.searchByAuthor(s);
                case "ISBN":
                    return new book[] {bm.searchByISBN(s)};
            }
            return new book[0];
        }


        private void button1_Click(object sender, EventArgs e)
        {
            BookManagerClient bm = new BookManagerClient();
            bm.addBook(new book{
                id=idField.Text,
                author=authorField.Text,
                isbn=isbnField.Text,
                pages= Int32.Parse(pagesField.Text),
                publisher=publisherField.Text,
                title=titleField.Text,
                year= Int32.Parse(yearField.Text)
            });
        }
    }
}
